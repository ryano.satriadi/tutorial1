import React from 'react';
 import {
   StyleSheet,
   Text,
   TextInput,
   View,
 } from 'react-native';
 

const Posttest = () => {
  return (
    <View style={{marginLeft : 20}, {marginTop :20} }>
      <Text style={styles.blue}>Registrasi</Text>
      <Text style={styles.grey}>Please enter details to register</Text>
      
      <TextInput placeholder="Input Nama" style={styles.textInput}/>
      <TextInput placeholder="Input User Name" style={styles.textInput}/>
      <TextInput placeholder="Input Email" style={styles.textInput}/>
      <TextInput placeholder="Input Nomor Telfon" style={styles.textInput}/>
      <TextInput placeholder="Input Input Pass" style={styles.textInput}/>
      <TextInput placeholder="Konfirmasi password" style={styles.textInput}/>

      <View style={styles.buttonContainer}>
        <Text style={styles.buttonText}>Sign up</Text>


      </View>
    <View style={{flexDirection: 'row'}}>
    <View style={{flex:1}}>
 


        </View>
        <View style={{flex:3, justifyContent: 'flex-start', flexDirection: 'row'}}>
        <Text>Already have account</Text><Text style={styles.signUpText}> Log in</Text>
        </View>
      </View>
    </View>
  )
}
 
const styles = StyleSheet.create({
  textInput: {borderWidth: StyleSheet.hairlineWidth, borderColor: 'blue', width: '90%', marginVertical: 5, borderRadius: 15},
  buttonContainer: {backgroundColor: 'blue', width: '90%', padding: 20, alignItems: 'center', borderRadius: 15, marginTop: 5},
  buttonText: {color: 'white',fontWeight: 'bold', fontSize: 20},
  signUpText: {color: 'blue',},
  blue: {color: 'blue', fontWeight: 'bold', fontSize: 40},
  grey: {color: 'grey', fontWeight: 'bold', fontSize: 15},
})

export default Posttest;